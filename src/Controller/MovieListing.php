<?php

namespace Drupal\unelmamovie_directory\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * MovieListing
 */

class MovieListing extends ControllerBase
{
    /**
     * view
     *
     * @return void
     */
    public function view()
    {
        $movies = $this->listMovies();
        $content = [];

        return [
        '#theme' => 'unelmamovie-listing',
        '#content' => $content,
        '#movies' => $movies,
        ];
    }

    /**
     * listMovies
     *
     * @return void
     */
    public function listMovies()
    {
        $movie_api_connector_service = \Drupal::service(id: 'unelmamovie_directory.api_connector');
        $movie_list = $movie_api_connector_service->browseMovies();

        if (!empty($movie_list->pagination->data)) {
            return $movie_list->pagination->data;
        }
        return [];
    }
}
