<?php

namespace Drupal\unelmamovie_directory\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * UnelmaMovieAPI
 */
class UnelmaMovieAPI extends FormBase
{

    const UNELMAMOVIE_API_CONFIG_PAGE = 'movie_api_config_page:values';
    
    /**
     * GetFormId
     *
     * @return void
     */
    public function getFormId()
    {
        return 'movie_api_config_page';
    }
    
    /**
     * BuildForm
     *
     * @param  mixed $form
     * @param  mixed $form_state
     * @return void
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $values = \Drupal::state()->get(key: self::UNELMAMOVIE_API_CONFIG_PAGE);
        $form = [];

        $form['api_base_url'] = [
        '#type' => 'textfield',
        '#title' => $this->t(string: 'API Base URL'),
        '#description' => $this->t(string: 'This is the API Base URL'),
        '#required' => true,
        '#default_value' => $values['api_base_url'],
        ];

        $form['api_key'] = [
        '#type' => 'textfield',
        '#title' => $this->t(string: 'API Key'),
        '#description' => $this->t(string: 'This is the api key used to access UnelmaMovie API'),
        '#required' => true,
        '#default_value' => $values['api_key'],
        ];

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t(string: 'Save'),
        '#button_type' => 'primary'
        ];

        return $form;
    }
    
    /**
     * SubmitForm
     *
     * @param  mixed $form
     * @param  mixed $form_state
     * @return void
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $submitted_values = $form_state->cleanValues()->getValues();

        \Drupal::state()->set(self::UNELMAMOVIE_API_CONFIG_PAGE, $submitted_values);
        $messenger = \Drupal::service(id: 'messenger');
        $messenger->addMessage($this->t(string: 'Your config has been saved'));
    }
}
