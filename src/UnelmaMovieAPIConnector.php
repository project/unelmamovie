<?php

namespace Drupal\unelmamovie_directory;

use GuzzleHttp\Client;
use Drupal\Core\StringTranslation\StringTranslationTrait; // Use this trait to access the t() function

/**
 * UnelmaMovieAPIConnector
 */
class UnelmaMovieAPIConnector
{
    use StringTranslationTrait; // Use this trait to access the t() function    
    /**
     * Client
     *
     * @var mixed
     */
    private $client;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $movie_api_config = \Drupal::state()
        ->get(key: \Drupal\unelmamovie_directory\Form\UnelmaMovieAPI::UNELMAMOVIE_API_CONFIG_PAGE);
        $api_key = ($movie_api_config['api_key']) ?: '';
        $this->client = new Client(['headers' => ['Authorization' => 'Bearer ' . $api_key]]);
    }
    
    /**
     * BrowseMovies
     *
     * @return void
     */
    public function browseMovies()
    {
        $movie_api_config = \Drupal::state()
        ->get(key: \Drupal\unelmamovie_directory\Form\UnelmaMovieAPI::UNELMAMOVIE_API_CONFIG_PAGE);
        $base_url = \Drupal\Core\Url::fromUri($movie_api_config['api_base_url'])->getUri();

        $data = [];
        $endpoint = $base_url . '/titles';

        $query = [
        'perPage' => 11,
        'page' => 1,
        'order' => 'popularity:desc',
        'genre' => 'action, comedy',
        'released' => '2019,2021',
        'runtime' => '120,240',
        'score' => '7,10',
        'language' => 'en',
        'certification' => 'pg-13',
        'country' => 'us',
        'onlyStreamable' => false,
        'includeAdult' => false
        ];
        $query = http_build_query($query);
        try {
            $response = $this->client->get($endpoint . '?' . $query);
            if ($response->getStatusCode() == 200) {
                // Success
                $result = $response->getBody()->getContents();
                $data = json_decode($result);
            } else {
                // Failure
                \Drupal::messenger()
                // Use the t() function from the trait
                ->addMessage($this->t('There was a problem connecting to the API.'), 'error');
            }
        } catch (\Exception $e) {
            // Error
            \Drupal::messenger()
            // Use the t() function from the trait
            ->addMessage($this->t('There was an error with your request.'), 'error');
            \Drupal::messenger()->addMessage($e->getMessage());
            if ($e->hasResponse()) {
                \Drupal::messenger()->addMessage($e->getResponse()->getBody());
            }
            \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('unelmamovie_directory'), $e), fn() => watchdog_exception('unelmamovie_directory', $e));
        }
        return $data;
    }
}