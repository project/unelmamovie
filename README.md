"UnelmaMovie_directory" module created in Drupal 10 is designed to provide a solution for managing and organizing movie directories within a Drupal-based website.

This module offers the following solutions:
Movie Catalog Management: It enables the creation of a comprehensive movie directory by allowing administrators to add, edit, and manage movie listings easily. It provides features such as title, genre, release year, director, cast, and other relevant information.

Note: a very basic module for educational purposes and shows only listing of movies

1. Install the module
2. You will have visit https://unelmamovie.com and grab a free API key
3. Paste URL and API token in your Drupal site by visiting following URL: /admin/config/unelmamovie-api
4. API Base URL is usually: https://unelmamovie.com/api/v1
5. API key can be found from UnelmaMovie.com website, from Account settings--> Developers -->Create token
6. Go to the drupal site /unelmamovie url, you should see the listing of some movies
